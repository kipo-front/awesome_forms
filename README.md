# README #

### Demo ###

![alt-text](form-demo.gif)

### Package Managers ###

* npm install git+ssh://git@bitbucket.org/kipo-front/awesome_forms.git
* keyPhrase: Kipo123$

### How do I get set up? ###

* include awesome_forms.scss in your main.scss like "node_modules/awesome_forms/lib/awesome_forms.scss";;
* import FormField from 'awesome_forms.js';
* jQuery

### CSS VARIABLES ###
```
:root {
  --color-form-field: #3d3c39;
  --color-form-field-error: #ca3610;
  --color-form-field-border: #d97b10;
  --color-form-field-label: #fc7317;
  --form-field-font-family: 'Roboto';
  --form-field-label-size: 16px;
  --form-field-label-active-size: 14px;

  --checkbox-width: auto;
  --checkbox-color: #ff6f06;
  --checkbox-tick-color: #fff;
  --checkbox-font-size: 16px;
  --checkbox-line-height: 16px;
  --checkbox-default-width: 18px;
  --checkbox-default-height: 18px;
  --checkbox-border-radius: 7px;
  --input-width: 16px;
  --input-height: 16px;

  --radio-width: auto;
  --radio-color: #ff6f06;
  --radio-circle-color: #fff;
  --radio-font-size: 16px;
  --radio-line-height: 16px;
  --radio-default-width: 18px;
  --radio-default-height: 18px;
  --radio-border-radius: 50%;
}
```

### HTML EXAMPLE ###
```
<div class="form-field">
  <div class="form-field-control">
    <label for="password{{ $section->id }}"
           class="form-field-label">Парола<span>*</span>
    </label>
    <input id="password{{ $section->id }}"
           name="password"
           type="password"
           class="form-field-input" />
    <div class="password-hide"></div>
  </div>
</div>
```
### JS EXAMPLE ###
```
    import FormField from 'awesome_forms';
    const setup = {
        formClassSelector: '.form',
        formFieldClassElements: ['.form-field-input', '.form-field-textarea'],
        formFieldActiveClass: 'form-field-is-active',
        formFieldFilledClass: 'form-field-is-filled',
        clearFormFields: false
    };
    const form = new FormField(setup);
    form.setFormFieldStyles();
```
### Settings ###

Option | Type | Default | Description
------ | ---- | ------- | -----------
formClassSelector | string | '.form' | CSS Class for form element |
formFieldClassElements | array of strings | ['.form-field-input', '.form-field-textarea'] | CSS classes for input and textarea 
formFieldActiveClass | string | 'form-field-is-active' | CSS class name for active mode
formFieldFilledClass | string | 'form-field-is-filled' | CSS class name for filled mode
clearFormFields | boolean | false | Clear all input fields after refresh
