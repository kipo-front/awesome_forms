let webpack = require('webpack');
let path = require('path');

let MiniCssExtractPlugin = require("mini-css-extract-plugin");
let UglifyJSPlugin = require('uglifyjs-webpack-plugin');

let autoprefixer = require('autoprefixer');

let publicPath = '/';

// let isProduction = (process.env.NODE_ENV == 'production');


const isProduction = process.env.NODE_ENV === "production";

module.exports = {
    mode: 'development',
    entry: {
        main: [
            path.resolve(__dirname, './main.js'),
            // path.resolve(__dirname, '../../sass/main.scss')
        ]
    },
    output: {
        path: path.resolve(__dirname + '../../../', 'public'), // Absolute path
        filename: '[name].min.js',
        publicPath: publicPath
    },
    module: {
        rules: [{
            // JS LOADER
            test: /\.js$/,
            exclude: /node_modules/,
            use: "babel-loader"
        },
            {
                // JS LOADER
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: { url: false }
                    },
                    {
                        loader: 'postcss-loader', options: {plugins: [autoprefixer({browsers: ['> 1%', 'last 4 versions', 'Firefox >= 20', 'iOS >=7']})]}
                    },
                    {
                        loader: 'sass-loader',
                        options: {}
                    }]
            }]
        // Loaders start from right to left -> sass-loader -> postcss-loader -> css-loader
    },
    optimization: {
        minimizer: [new UglifyJSPlugin({cache: true})]
    },
    plugins: [
        new MiniCssExtractPlugin({filename: "[name].min.css"}),
        new webpack.LoaderOptionsPlugin({ minimize: true }),
        new webpack.ProvidePlugin({
            jQuery: 'jquery',
            $: 'jquery',
            jquery: 'jquery',
            'window.jQuery': 'jquery'
        })
    ]
};
module.exports.plugins.push(new UglifyJSPlugin());
// if(isProduction) {
//     module.exports.plugins.push(new UglifyJSPlugin());
// }
