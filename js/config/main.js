const defaultSetup = {
    formClassSelector: '.form',
    formFieldClassElements: ['.form-field-input', '.form-field-textarea'],
    formFieldActiveClass: 'form-field-is-active',
    formFieldFilledClass: 'form-field-is-filled',
    clearFormFields: false
};


class FormField {
    constructor(setup = {...defaultSetup}) {
        this._initialSetup = {};
        this._initialSetup.formClassSelector = setup.formClassSelector || defaultSetup.formClassSelector;
        this._initialSetup.formFieldClassElements = setup.formFieldClassElements || defaultSetup.formFieldClassElements;
        this._initialSetup.formFieldActiveClass = setup.formFieldActiveClass || defaultSetup.formFieldActiveClass;
        this._initialSetup.formFieldFilledClass = setup.formFieldFilledClass || defaultSetup.formFieldFilledClass;
        this._initialSetup.clearFormFields = setup.clearFormFields || defaultSetup.clearFormFields;
        if(this._initialSetup.clearFormFields) {
            this.clearFields();
        }

        this._setFormFieldStyles();
    }
    _setFormFieldStyles() {
        const setActive = (el, active) => {
            const formField = el.parentNode.parentNode;
            if (active) {

                formField.classList.add(this._initialSetup.formFieldActiveClass)
            } else {
                formField.classList.remove(this._initialSetup.formFieldActiveClass);
                el.value === '' ?
                    formField.classList.remove(this._initialSetup.formFieldFilledClass) :
                    formField.classList.add(this._initialSetup.formFieldFilledClass)
            }
        };

        [].forEach.call(
            document.querySelectorAll(this._initialSetup.formFieldClassElements.join(', ')),
            (el) => {
                el.onblur = () => {
                    setActive(el, false)
                };
                el.onfocus = () => {
                    setActive(el, true)
                }
            }
        );

        $('.form-field-input, .form-field-textarea').each((indx, el) => {
            if($(el).val() !== '') {
                $(el).parent().parent().addClass(this._initialSetup.formFieldFilledClass)
            }
        })
    }

    initTogglePasswordVisibility() {
        this.togglePasswordTypeField();
    }
    togglePasswordTypeField() {
        const togglePassword = $('.password-hide');

        togglePassword.on('click', function() {
            const passwordFieldType = $(this).parent().find('input').attr('type');
            if(passwordFieldType === 'password') {
                $(this).parent().find('input').attr('type', 'text');
                $(this).addClass('password-visible');
                $(this).removeClass('password-hide');
            } else {
                $(this).parent().find('input').attr('type', 'password');
                $(this).removeClass('password-visible');
                $(this).addClass('password-hide');

            }

        })
    }

    clearFields() {
        const form = $(this._initialSetup.formClassSelector);
        form.find('input:not([type="checkbox"])').each(function () {
            $(this).val('');
        });

        form.find('textarea').each(function () {
            $(this).val('');
        });
        form.find('[type="checkbox"]').prop('checked', false);
    }
}




export default FormField;

