// import FormField from './js/config/index.js';
//
// console.log(FormField);
//     const setup = {
//         formClassSelector: '.form',
//         formFieldClassElements: ['.form-field-input', '.form-field-textarea'],
//         formFieldActiveClass: 'form-field-is-active',
//         formFieldFilledClass: 'form-field-is-filled',
//         clearFormFields: false
//     };
//     const form = new FormField(setup);
//     console.log(new FormField());


import FormField from './js/config/main.js';
import lozad from './js/config/node_modules/lozad/dist/lozad.es.js'
const setup = {
    formClassSelector: '.form',
    formFieldClassElements: ['.form-field-input', '.form-field-textarea'],
    formFieldActiveClass: 'form-field-is-active',
    formFieldFilledClass: 'form-field-is-filled',
    clearFormFields: false
};
const form = new FormField(setup);
console.log(lozad)


